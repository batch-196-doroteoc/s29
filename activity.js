//add 5 courses

db.courses.insertMany(
    [
        {
          name: "HTML Basics",
          price: 20000,
          isActive: true,
          instructor: "Sir Alvin"
        },

        {
          name: "CSS 101 + Flexbox",
          price: 21000,
          isActive: true,
          instructor: "Sir Alvin"
        },

        {
          name: "Javascript 101",
          price: 32000,
          isActive: true,
          instructor: "Ma'am Tine"
        },
        {
          name: "Git 101, IDE and CLI",
          price: 19000,
          isActive: false,
          instructor: "Ma'am Tine"
        },
        {
          name: "React.Js 101",
          price: 25000,
          isActive: true,
          instructor: "Ma'am Miah"
        },
    ]

)

//find - instructor:alvin, price: gte20000

db.courses.find({$and:[{instructor:{$regex: 'alvin', $options: '$i'}},{price:{$gte:20000}}]})

//find - instructor:tine, isActive: false

db.courses.find({$and:[{instructor:{$regex: 'tine', $options: '$i'}},{isActive:false}]})

//find - name:r, price lte:25000

db.courses.find({$and:[{name:{$regex: 'r', $options: '$i'}},{price:{$lte:25000}}]})

//update - price: lt 21000 set isActive: false

db.courses.updateMany({price:{$lt:21000}},{$set:{isActive:false}})

//delete - price: gte 25000

db.courses.deleteMany({price:{$gte:25000}})



// db.courses.insertMany(
//     [
//         {
//           name: "HTML Basics",
//           price: 20000,
//           isActive: true,
//           instructor: "Sir Alvin"
//         },
// 
//         {
//           name: "CSS 101 + Flexbox",
//           price: 21000,
//           isActive: true,
//           instructor: "Sir Alvin"
//         },
// 
//         {
//           name: "Javascript 101",
//           price: 32000,
//           isActive: true,
//           instructor: "Ma'am Tine"
//         },
//         {
//           name: "Git 101, IDE and CLI",
//           price: 19000,
//           isActive: false,
//           instructor: "Ma'am Tine"
//         },
//         {
//           name: "React.Js 101",
//           price: 25000,
//           isActive: true,
//           instructor: "Ma'am Miah"
//         },
//     ]
// 
// )   
// db.courses.find({$and:[{instructor:{$regex: 'alvin', $options: '$i'}},{price:{$gte:20000}}]})
// db.courses.find({$and:[{instructor:{$regex: 'tine', $options: '$i'}},{isActive:false}]})
// db.courses.find({$and:[{name:{$regex: 'r', $options: '$i'}},{price:{$lte:25000}}]})
// db.courses.updateMany({price:{$lt:21000}},{$set:{isActive:false}})
//db.courses.deleteMany({price:{$gte:25000}})